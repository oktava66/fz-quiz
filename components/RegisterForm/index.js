import axios from 'axios';
import cn from 'classnames';
import { useState, useCallback, useEffect } from 'react';
import { useLocalStorage } from '../../utils/hooks';

const RegisterForm = ({ onRegistered }) => {
  const [LSNickname, setLSNickname] = useLocalStorage('nickname', '');
  const [nickname, setNickname] = useState(LSNickname);
  const [pending, setPending] = useState(false);
  const [error, setError] = useState(false);
  const postNickname = useCallback((nickname) => {
    if (pending) return;
    setPending(true);

    axios.post('/registration/', { nickname })
      .then(() => {
        setLSNickname(nickname);
        onRegistered(nickname);
      })
      .catch(() => {
        setError(true);
        setPending(false);
      });
  }, [pending]);

  useEffect(() => {
    if (LSNickname) {
      postNickname(LSNickname);
    }
  }, []);

  return (
    <div>
      <h1>WELCOME TO QUIZ</h1>
      <img src="/static/fz-logo.svg" />
      <form onSubmit={(e) => {
        e.preventDefault();
        setError(false);
        if (nickname) postNickname(nickname);
        else setError(true);
      }}>
        <label>Введите ваш ник</label>
        <input
          className={cn({ error })}
          type="text"
          data-error={error.toString()}
          value={nickname}
          onChange={(e) => {
            if (error) setError(false);
            setNickname(e.target.value);
          }}
        />
        <button>
          Начать игру
        </button>
      </form>
      <style jsx>{`
        div {
          text-align: center;
        }
        h1 {
          margin-bottom: 60px;
          font-size: 30px;
          line-height: 36px;
          font-weight: 600;
        }
        img {
          width: 100%;
          max-width: 220px;
          margin-bottom: 60px;
        }
        label {
          display: block;
          margin-bottom: 5px;
          font-size: 16px;
        }
        input {
          width: 100%;
          padding: 10px 18px;
          margin-bottom: 60px;
          
          text-align: center;
          font-size: 21px;
          line-height: 25px;
          color: #000;
          
          border-radius: 25px;
          border: none;
          outline: none;
        }
        input.error {
          background: #ffebeb;
          border: 3px solid #ff4141;
          margin-top: -3px;
          animation: error .2s linear 3;
        }
        button {
          width: 100%;
          padding: 0;
          
          font-size: 17px;
          line-height: 60px;
          
          border: none;
          border-radius: 30px;
          box-shadow: 0px 4px 10px -3px rgba(0, 0, 0, 0.75);
          background: #fff;
          outline: none;
          transition: box-shadow .15s, transform .15s;
        }
        button:active {
          box-shadow: 0px 0px 3px -1px rgba(0, 0, 0, 0.75);
          transform: translateY(1px);
        }
        @keyframes error {
          0% {transform: translateX(0); }
          25% {transform: translateX(15px); }
          75% {transform: translateX(-15px); }
          100% {transform: translateX(0); }
        }
      `}</style>
    </div>
  );
};

export default RegisterForm;
