const AdminControls = ({
  actions: {
    onQuizStart,
    onSubmitQuestion,
    onNextQuestion,
    onResetQuiz,
  },
  answer,
  step,
}) => (
  <div>
    <button onClick={onQuizStart} disabled={step !== 'waiting'}>
      Start quiz
    </button>
    <button onClick={onSubmitQuestion} disabled={step !== 'game' || typeof answer === "number"}>
      Submit question
    </button>
    <button onClick={onNextQuestion} disabled={step !== 'game' || typeof answer !== "number"}>
      Next question
    </button>
    <button onClick={onResetQuiz}>
      Reset quiz
    </button>
    <style jsx>{`
      div {
        position: fixed;
        bottom: 5px;
        left: 5px;
      }
      button {
        margin-right: 5px;
      }
    `}</style>
  </div>
);

export default AdminControls;
