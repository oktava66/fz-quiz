class Timer extends React.Component {
  timeoutId = null;
  intervalId = null;

  state = {
    timeout: 0,
  };

  componentDidMount() {
    window.addEventListener('focus', this.onWindowFocus, false);

    this.runTimer();
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutId);
    clearInterval(this.intervalId);

    window.removeEventListener('focus', this.onWindowFocus, false);
  }

  onWindowFocus = () => {
    clearTimeout(this.timeoutId);
    clearInterval(this.intervalId);

    this.runTimer();
  };

  runTimer = () => {
    const { timeoutDate } = this.props;
    const timeoutMS = new Date(timeoutDate).getTime() - new Date().getTime();
    const delay = timeoutMS % 1000;
    let timeout = (timeoutMS - delay) / 1000;

    this.setState({ timeout });

    this.timeoutId = setTimeout(() => {
      this.intervalId = setInterval(() => {
        timeout -= 1;
        this.setState({ timeout });

        if (timeout <= 0) {
          clearInterval(this.intervalId);
        }
      }, 1000);

    }, timeout);
  };

  render() {
    const { timeout } = this.state;
    const minutes = Math.floor(timeout / 60);
    const seconds = timeout - minutes * 60;

    return (
      <span>
        {minutes}:{seconds > 9 ? '' : '0'}{seconds}
      </span>
    );
  }
}

export default Timer;
