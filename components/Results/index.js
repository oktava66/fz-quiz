const getHighestScore = (score) => {
  let highestScore = 0;

  Object.keys(score).forEach((nickname) => {
    if (score[nickname] && score[nickname] > highestScore) {
      highestScore = score[nickname];
    }
  });

  return highestScore;
};

const getWinners = (score, highestScore) => {
  let winners = [];

  Object.keys(score).forEach((nickname) => {
    if (score[nickname] === highestScore) {
      winners.push(nickname);
    }
  });

  return winners;
};

const Results = ({ score, connectedUsers }) => {
  const highestScore = getHighestScore(score);
  const winners = getWinners(score, highestScore);
  const nickname = (localStorage.getItem('nickname') || '').split('"').join('');

  return (
    <div>
      {winners.length > 0 ? (
        <>
          <h1>ПОЗДРАВЛЯЕМ</h1>
          <h2>
            {winners.length === 1 ? 'Наш победитель' : 'Наши победители'}
            {' – '}
            {winners.join(', ')}
            <br />
            Результат – {highestScore}
          </h2>

          <div className="avatars">
            {Object.keys(connectedUsers).map((nickname, key) => {
              if (!winners.includes(connectedUsers[nickname])) {
                return null;
              }
              return (
                <div className="avatar">
                  <img
                    src={`/static/icons/${connectedUsers[nickname].avatar}`}
                    key={key}
                  />
                </div>
              );
            })}
          </div>

          <img
            className="celebration"
            src="/static/celebration.svg"
          />

          {score[nickname] && (
            <h3>Ваш результат – {score[nickname]}</h3>
          )}
        </>
      ) : (
        <>
          <h1>Сегодня нет победителя :(</h1>
        </>
      )}
      <style jsx>{`
        div {
          text-align: center;
        }
        h1 {
          font-size: 35px;
          margin-bottom: 40px;
        }
        h2 {
          font-size: 25px;
          line-height: 28px;
          margin-bottom: 40px
        }
        .avatars {
          display: flex;
          flex-direction: row;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;
          margin-bottom: 40px;
        }
        .avatar {
          display: flex;
          justify-content: center;
          align-items: center;
          flex-shrink: 0;
          width: 80px;
          height: 80px;
          background: #fff;
          border-radius: 50px;
          margin: 10px;
          box-shadow: 0 4px 15px -4px rgba(0, 0, 0, 0.6);
        }
        .avatar img {
          width: 55px;
        }
        
        .celebration {
          max-width: 270px;
          margin-bottom: 40px;
        }
      `}</style>
    </div>
  );
};

export default Results;
