const WaitingScreen = ({ connectedUsers }) => (
  <>
    <h1>Игроки</h1>
    <ul>
      {Object.keys(connectedUsers).map((nickname, key) => (
        <li key={key}>
          <span className="imageWrap">
            <img src={`/static/icons/${connectedUsers[nickname].avatar}`} />
          </span>
          <span>
            {nickname}
          </span>
        </li>
      ))}
    </ul>
    <style jsx>{`
      h1 {
        font-size: 30px;
        font-weight: 600;
        margin-bottom: 40px;
      }
      li {
        margin-bottom: 30px;
      
        display: flex;
        justify-content: flex-start;
        align-items: center;
        
        font-size: 21px;
        line-height: 25px;
      }
      .imageWrap {
        width: 80px;
        height: 80px;
        margin-right: 20px;
        
        display: flex;
        align-items: center;
        justify-content: center;
        flex-shrink: 0;
        
        background: #fff;
        border-radius: 50%;
        box-shadow: 0 4px 15px -4px rgba(0, 0, 0, 0.6);
      }
      img {
        width: 55px;
      }
    `}</style>
  </>
);


export default WaitingScreen;
