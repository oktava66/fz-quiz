import cn from 'classnames';

import Timer from '../Timer';

const isNum = n => typeof n === 'number';

class Game extends React.Component {
  render() {
    const { question, timeoutDate, answer, selected, onAnswerSelect } = this.props;

    if (!question) return null;

    return (
      <div>
        <h1>
          {timeoutDate ? (
            <Timer
              timeoutDate={timeoutDate}
            />
          ) : 'Правильный ответ'}
        </h1>
        <p dangerouslySetInnerHTML={{ __html: question.text }} />
        {question.options.map((item, key) => (
          <button
            key={key}
            onClick={() => {
              if (isNum(answer)) return;
              onAnswerSelect(key);
            }}
            className={cn(
              'button',
              isNum(selected) && !isNum(answer) && selected !== key && 'deselected',
              key === answer && 'answer',
              key === selected && isNum(answer) && answer !== selected && 'wrong',
            )}
          >
            <div dangerouslySetInnerHTML={{ __html: item }} />
          </button>
        ))}
        <style jsx>{`
        h1 {
          position: fixed;
          left: 0;
          right: 0;
          top: 0;
          padding: 6px;
          
          font-size: 21px;
          text-align: center;
          
          box-shadow: 0 0px 10px -4px #000;
          background: #FA9D44;
        }
        p {
          margin: 40px auto 20px;
        
          font-size: 21px;
          line-height: 25px;
        }
        :global(code) {
          display: block;
          margin: 20px -10px;
          padding: 10px;
          overflow: auto;
          
          font: 17px/23px Courier, serif;
          white-space: nowrap;
          
          background: rgba(8, 8, 8, 0.85);
          border-radius: 6px;
        }
        :global(b) {
          font: 21px/24px Courier, serif;
        }
        .button {
          display: block;
          width: 100%;
          padding: 20px;
          margin-bottom: 20px;
          
          font-size: 20px;
          line-height: 24px;
          
          border: none;
          border-radius: 8px;
          background: #fff;
          outline: none;
          cursor: pointer;
        }
        .deselected {
          opacity: .4;
        }
        .answer {
          background: #4ADA63;
        }
        .wrong {
          background: #EB3030;
        }
      `}</style>
      </div>
    );
  }
}

export default Game;
