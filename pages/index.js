import Head from 'next/head';

import Layout from '../components/Layout';
import RegisterForm from '../components/RegisterForm';
import WaitingScreen from '../components/WaitingScreen';
import Game from '../components/Game';
import Results from '../components/Results';
import AdminControls from '../components/AdminControls'
import quizController from '../containers/quizController';

class Home extends React.Component {
  render() {
    const {
      step,
      actions,
      connectedUsers,
      score,
      question,
      timeoutDate,
      selected,
      answer,
      isAdmin,
    } = this.props;

    return (
      <Layout>
        <Head>
          <title>Frontend Quiz 🚀</title>
          <link rel="icon" type="image/png" href="/static/icons/bot-4.png" />
        </Head>

        {step === 'registration' && (
          <RegisterForm
            onRegistered={actions.onRegistered}
          />
        )}
        {step === 'waiting' && (
          <WaitingScreen
            connectedUsers={connectedUsers}
          />
        )}
        {step === 'game' && (
          <Game
            question={question}
            timeoutDate={timeoutDate}
            selected={selected}
            answer={answer}
            onAnswerSelect={actions.onAnswerSelect}
          />
        )}
        {step === 'finished' && (
          <Results
            score={score}
            connectedUsers={connectedUsers}
          />
        )}
        {isAdmin && (
          <AdminControls
            actions={actions}
            answer={answer}
            step={step}
          />
        )}
      </Layout>
    );
  }
}

export default quizController(Home);
