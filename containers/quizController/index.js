import io from 'socket.io-client/dist/socket.io.js';

function quizController(WrappedComponent) {
  return class extends React.Component {
    state = {
      connectedUsers: {},
      score: undefined,
      question: null,
      selected: null,
      answer: null,
      step: null,
      isAdmin: false,
    };

    componentDidMount() {
      this.setState({ step: 'registration' });

      if (document.cookie.includes('quiz_credentials=')) {
        this.setState({ isAdmin: true });
      }
    }

    onRegistered = (nickname) => {
      this.setState({ nickname }, this.initSocketConnection);
    };

    onAnswerSelect = (selected) => {
      if (this.state.selected === selected) {
        return;
      }

      this.socket.emit('answer_select', selected, () => {
        this.setState({ selected });
      });
    };

    onQuizStart = () => {
      this.socket.emit('start_game')
    };

    onNextQuestion = () => {
      this.socket.emit('next_question')
    };

    onSubmitQuestion = () => {
      this.socket.emit('submit_question')
    };

    onResetQuiz = () => {
      this.socket.emit('reset_game');
    };

    initSocketConnection = () => {
      this.socket = io();
      this.socket.on('connected', ({ step, question, timeoutDate, answer, score, selected }) => {
        this.setState({ step, question, timeoutDate, answer, score, selected });
      });
      this.socket.on('connected_users_update', (connectedUsers) => {
        this.setState({ connectedUsers });
      });
      this.socket.on('step_update', (step) => {
        this.setState({ step });
      });
      this.socket.on('submit_question', (answer) => {
        this.setState({
          answer,
          timeoutDate: null,
        });
      });
      this.socket.on('next_question', (question, timeoutDate) => {
        this.setState({
          question,
          timeoutDate,
          answer: null,
          selected: null,
        });
      });
      this.socket.on('quiz_finished', (score) => {
        this.setState({ score });
      });

      this.socket.emit('registered', this.state.nickname, (selected) => {
        this.setState({ selected });
      });
    };

    render() {
      const {
        step,
        connectedUsers,
        score,
        question,
        timeoutDate,
        selected,
        answer,
        isAdmin,
      } = this.state;


      return (
        <WrappedComponent
          step={step}
          connectedUsers={connectedUsers}
          score={score}
          question={question}
          timeoutDate={timeoutDate}
          selected={selected}
          answer={answer}
          isAdmin={isAdmin}
          actions={{
            onRegistered: this.onRegistered,
            onQuizStart: this.onQuizStart,
            onNextQuestion: this.onNextQuestion,
            onSubmitQuestion: this.onSubmitQuestion,
            onResetQuiz: this.onResetQuiz,
            onAnswerSelect: this.onAnswerSelect,
          }}
          {...this.props}
        />
      );
    }
  };
}

export default quizController;
