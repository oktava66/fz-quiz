const express = require('express');
const path = require('path');
const next = require('next');
const bodyParser = require('body-parser');
const createServer = require('http').createServer;
const socketIO = require('socket.io');
const quizController = require('./quizController');

const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== 'production';

const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();


nextApp.prepare().then(() => {
  const app = express();
  const http = createServer(app);
  const io = socketIO(http);

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  quizController(io, app);

  app.use('/static', express.static(path.join(__dirname, 'static')));

  app.get('*', (req, res) => {
    return handle(req, res);
  });

  http.listen(PORT, err => {
    if (err) throw err;
  })
});
