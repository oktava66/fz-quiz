const questions = require('./questions.json');
const cookieParser = require('cookie');

const SUPER_SECURE_CREDENTIALS = 'VWx0cmEgc2VjdXJlIHBhc3N3b3JkISEh';

const connectedUsers = {};
const avatars = {};
let selections = {};
let score = {};
let step = 'waiting';
let questionIndex = -1;
let timeoutDate = null;
let timeoutId = null;
let rightAnswerKey = null;

const doesUserExists = (nickname) => (
  !!Object.keys(connectedUsers).find(key => connectedUsers[key] === nickname)
);

const getQuestion = () => {
  const question = questions[questionIndex];
  return question ? {
    text: question.text,
    options: question.options,
    timeout: question.timeout,
  } : null;
};

const submitQuestion = (io) => {
  const key = questions[questionIndex].key;

  Object.keys(selections).forEach((nickname) => {
    if (selections[nickname] === key) {
      score[nickname] = (score[nickname] || 0) + 1;
    }
  });

  io.emit('submit_question', key);
  selections = {};
  rightAnswerKey = key;
  timeoutDate = null;
};

const releaseNextQuestion = (io) => {
  const question = getQuestion();

  timeoutDate = new Date(new Date().getTime() + question.timeout * 1000);
  timeoutId = setTimeout(() => {
    submitQuestion(io);
  }, question.timeout * 1000);

  io.emit('next_question', question, timeoutDate);
};

const getNormalizedConnectedUsers = () => {
  const users = {};
  Object.keys(connectedUsers).forEach((userId) => {
    const nickname = connectedUsers[userId];
    users[nickname] = {
      avatar: avatars[nickname],
      score: score[nickname],
    };
  });
  return users;
};

function quizController(io, app) {
  io.on('connection', (socket) => {
    socket.emit('connected', {
      step,
      timeoutDate,
      score,
      answer: rightAnswerKey,
      question: getQuestion(),
      selected: selections[connectedUsers[socket.id]] || null,
    });

    socket.on('registered', (nickname, cb) => {
      connectedUsers[socket.id] = nickname;
      avatars[nickname] = avatars[nickname] || `bot-${Object.keys(avatars).length % 18}.png`;

      io.emit('connected_users_update', getNormalizedConnectedUsers());

      if (selections[nickname]) {
        cb(selections[nickname]);
      }
    });

    socket.on('answer_select', (select, cb) => {
      selections[connectedUsers[socket.id]] = select;
      cb();
    });

    socket.on('disconnect', () => {
      delete connectedUsers[socket.id];

      io.emit('connected_users_update', getNormalizedConnectedUsers());
    });

    // Specifying the events just for quiz administrator

    try {
      if (cookieParser.parse(socket.handshake.headers.cookie).quiz_credentials !== SUPER_SECURE_CREDENTIALS) {
        return;
      }
    } catch (e) {
      return;
    }

    socket.on('start_game', () => {
      questionIndex += 1;

      releaseNextQuestion(io);

      step = 'game';
      io.emit('step_update', step);
    });

    socket.on('submit_question', () => {
      clearTimeout(timeoutId);
      submitQuestion(io);
    });

    socket.on('next_question', () => {
      selections = {};
      questionIndex += 1;
      rightAnswerKey = null;

      if (questions[questionIndex]) {
        releaseNextQuestion(io);
      } else {
        io.emit('quiz_finished', score);

        step = 'finished';
        io.emit('step_update', step);
      }
    });

    socket.on('reset_game', () => {
      score = {};
      selections = {};
      step = 'waiting';
      questionIndex = -1;
      rightAnswerKey = null;

      timeoutDate = null;
      clearTimeout(timeoutId);

      io.emit('step_update', step);
      io.emit('new_question', null);
      io.emit('submit_question', null);
    });
  });

  app.post('/registration/', (req, res) => {
    const nickname = req.body.nickname;

    if (doesUserExists(nickname)) {
      return res.status(452).json({
        text: 'user already exists'
      });
    }

    res.end();
  });
}

module.exports = quizController;
